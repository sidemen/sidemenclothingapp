package com.sidemenclothing.app

import androidx.multidex.MultiDexApplication
import com.sidemenclothing.app.conf.timberConf
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class CustomApplication : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()
        timberConf()
    }
}