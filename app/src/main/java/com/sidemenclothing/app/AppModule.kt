package com.sidemenclothing.app

import com.sidemenclothing.domain.di.AppFlavor
import com.sidemenclothing.domain.di.AppVersionName
import com.sidemenclothing.domain.model.Flavor
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @AppFlavor
    fun provideFlavor(): Flavor = Flavor.getFlavorFromCode(BuildConfig.FLAVOR)

    @Provides
    @AppVersionName
    fun provideAppName(@AppFlavor flavor: Flavor): String {
        return when (flavor) {
            Flavor.PROD -> BuildConfig.VERSION_NAME_MINOR
            else -> BuildConfig.VERSION_NAME
        }
    }
}