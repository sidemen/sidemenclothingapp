package com.sidemenclothing.data.entity.navigation

import com.sidemenclothing.data.entity.navigation.SocialMediaData.FACEBOOK
import com.sidemenclothing.data.entity.navigation.SocialMediaData.INSTAGRAM
import com.sidemenclothing.data.entity.navigation.SocialMediaData.TWITTER
import com.sidemenclothing.data.entity.navigation.SocialMediaData.YOUTUBE
import com.sidemenclothing.domain.model.navigation.SocialMedia

enum class SocialMediaData(val url: String) {
    FACEBOOK("https://www.facebook.com/SidemenClothing/"),
    INSTAGRAM("https://www.instagram.com/sidemenclothing/"),
    TWITTER("https://twitter.com/SidemenClothing/"),
    YOUTUBE("https://www.youtube.com/sidemen?sub_confirmation=1/")
}

fun SocialMediaData.mapToDomain() = when (this) {
    FACEBOOK -> SocialMedia.FACEBOOK
    INSTAGRAM -> SocialMedia.INSTAGRAM
    TWITTER -> SocialMedia.TWITTER
    YOUTUBE -> SocialMedia.YOUTUBE
}

fun SocialMedia.mapToData() = when (this) {
    SocialMedia.FACEBOOK -> FACEBOOK
    SocialMedia.INSTAGRAM -> INSTAGRAM
    SocialMedia.TWITTER -> TWITTER
    SocialMedia.YOUTUBE -> YOUTUBE
}