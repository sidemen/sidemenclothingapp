package com.sidemenclothing.data.entity.navigation

import com.sidemenclothing.data.entity.navigation.SubMenuItemProductTagData.ACCESSORIES_SALE
import com.sidemenclothing.data.entity.navigation.SubMenuItemProductTagData.ALL
import com.sidemenclothing.data.entity.navigation.SubMenuItemProductTagData.BAGS
import com.sidemenclothing.data.entity.navigation.SubMenuItemProductTagData.CUSHIONS
import com.sidemenclothing.data.entity.navigation.SubMenuItemProductTagData.GAMING
import com.sidemenclothing.data.entity.navigation.SubMenuItemProductTagData.HEADWEAR
import com.sidemenclothing.data.entity.navigation.SubMenuItemProductTagData.HOODIES_SWEATSHIRTS
import com.sidemenclothing.data.entity.navigation.SubMenuItemProductTagData.JACKETS_COATS
import com.sidemenclothing.data.entity.navigation.SubMenuItemProductTagData.JEWELLERY
import com.sidemenclothing.data.entity.navigation.SubMenuItemProductTagData.KIDS_SALE
import com.sidemenclothing.data.entity.navigation.SubMenuItemProductTagData.LEGGINGS
import com.sidemenclothing.data.entity.navigation.SubMenuItemProductTagData.MENS_SALE
import com.sidemenclothing.data.entity.navigation.SubMenuItemProductTagData.ONESIES
import com.sidemenclothing.data.entity.navigation.SubMenuItemProductTagData.PHONE
import com.sidemenclothing.data.entity.navigation.SubMenuItemProductTagData.POSTERS
import com.sidemenclothing.data.entity.navigation.SubMenuItemProductTagData.SHORTS
import com.sidemenclothing.data.entity.navigation.SubMenuItemProductTagData.TRACKSUITS
import com.sidemenclothing.data.entity.navigation.SubMenuItemProductTagData.T_SHIRTS
import com.sidemenclothing.data.entity.navigation.SubMenuItemProductTagData.WINTER
import com.sidemenclothing.data.entity.navigation.SubMenuItemProductTagData.WOMENS_SALE
import com.sidemenclothing.domain.model.navigation.SubMenuItemProductTag

enum class SubMenuItemProductTagData {
    ALL,
    HOODIES_SWEATSHIRTS,
    T_SHIRTS,
    JACKETS_COATS,
    TRACKSUITS,
    LEGGINGS,
    SHORTS,
    ONESIES,
    PHONE,
    HEADWEAR,
    WINTER,
    BAGS,
    GAMING,
    JEWELLERY,
    POSTERS,
    CUSHIONS,
    MENS_SALE,
    WOMENS_SALE,
    KIDS_SALE,
    ACCESSORIES_SALE
}

fun SubMenuItemProductTagData.mapToDomain() = when (this) {
    ALL -> SubMenuItemProductTag.ALL
    HOODIES_SWEATSHIRTS -> SubMenuItemProductTag.HOODIES_SWEATSHIRTS
    T_SHIRTS -> SubMenuItemProductTag.T_SHIRTS
    JACKETS_COATS -> SubMenuItemProductTag.JACKETS_COATS
    TRACKSUITS -> SubMenuItemProductTag.TRACKSUITS
    LEGGINGS -> SubMenuItemProductTag.LEGGINGS
    SHORTS -> SubMenuItemProductTag.SHORTS
    ONESIES -> SubMenuItemProductTag.ONESIES
    PHONE -> SubMenuItemProductTag.PHONE
    HEADWEAR -> SubMenuItemProductTag.HEADWEAR
    WINTER -> SubMenuItemProductTag.WINTER
    BAGS -> SubMenuItemProductTag.BAGS
    GAMING -> SubMenuItemProductTag.GAMING
    JEWELLERY -> SubMenuItemProductTag.JEWELLERY
    POSTERS -> SubMenuItemProductTag.POSTERS
    CUSHIONS -> SubMenuItemProductTag.CUSHIONS
    MENS_SALE -> SubMenuItemProductTag.MENS_SALE
    WOMENS_SALE -> SubMenuItemProductTag.WOMENS_SALE
    KIDS_SALE -> SubMenuItemProductTag.KIDS_SALE
    ACCESSORIES_SALE -> SubMenuItemProductTag.ACCESSORIES_SALE
}

fun SubMenuItemProductTag.mapToData() = when (this) {
    SubMenuItemProductTag.ALL -> ALL
    SubMenuItemProductTag.HOODIES_SWEATSHIRTS -> HOODIES_SWEATSHIRTS
    SubMenuItemProductTag.T_SHIRTS -> T_SHIRTS
    SubMenuItemProductTag.JACKETS_COATS -> JACKETS_COATS
    SubMenuItemProductTag.TRACKSUITS -> TRACKSUITS
    SubMenuItemProductTag.LEGGINGS -> LEGGINGS
    SubMenuItemProductTag.SHORTS -> SHORTS
    SubMenuItemProductTag.ONESIES -> ONESIES
    SubMenuItemProductTag.PHONE -> PHONE
    SubMenuItemProductTag.HEADWEAR -> HEADWEAR
    SubMenuItemProductTag.WINTER -> WINTER
    SubMenuItemProductTag.BAGS -> BAGS
    SubMenuItemProductTag.GAMING -> GAMING
    SubMenuItemProductTag.JEWELLERY -> JEWELLERY
    SubMenuItemProductTag.POSTERS -> POSTERS
    SubMenuItemProductTag.CUSHIONS -> CUSHIONS
    SubMenuItemProductTag.MENS_SALE -> MENS_SALE
    SubMenuItemProductTag.WOMENS_SALE -> WOMENS_SALE
    SubMenuItemProductTag.KIDS_SALE -> KIDS_SALE
    SubMenuItemProductTag.ACCESSORIES_SALE -> ACCESSORIES_SALE
}
