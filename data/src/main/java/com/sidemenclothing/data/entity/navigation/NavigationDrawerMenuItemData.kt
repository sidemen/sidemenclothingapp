package com.sidemenclothing.data.entity.navigation

data class NavigationDrawerMenuItemData(
    val title: MenuItemTagData,
    val subMenu: List<NavigationDrawerSubMenuItemData>
)