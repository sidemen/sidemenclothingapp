package com.sidemenclothing.data.entity.navigation

import com.sidemenclothing.data.entity.navigation.SubMenuItemCollectionTagData.MMXIX
import com.sidemenclothing.data.entity.navigation.SubMenuItemCollectionTagData.SDMN_ACTIVEWEAR
import com.sidemenclothing.data.entity.navigation.SubMenuItemCollectionTagData.SDMN_CLASSIC
import com.sidemenclothing.data.entity.navigation.SubMenuItemCollectionTagData.SDMN_COLOUR_BLOCK
import com.sidemenclothing.data.entity.navigation.SubMenuItemCollectionTagData.SDMN_HAZARD
import com.sidemenclothing.data.entity.navigation.SubMenuItemCollectionTagData.SDMN_MOO_OFF
import com.sidemenclothing.data.entity.navigation.SubMenuItemCollectionTagData.SDMN_TWO_TONE
import com.sidemenclothing.data.entity.navigation.SubMenuItemCollectionTagData.SDMN_WINTER_COLLECTION
import com.sidemenclothing.data.entity.navigation.SubMenuItemCollectionTagData.SIDEMEN_ARCADE
import com.sidemenclothing.data.entity.navigation.SubMenuItemCollectionTagData.SIDENET
import com.sidemenclothing.data.entity.navigation.SubMenuItemCollectionTagData.XIX_WORLD
import com.sidemenclothing.domain.model.navigation.SubMenuItemCollectionTag

enum class SubMenuItemCollectionTagData {
    SIDEMEN_ARCADE,
    SDMN_WINTER_COLLECTION,
    XIX_WORLD,
    SDMN_COLOUR_BLOCK,
    SDMN_ACTIVEWEAR,
    SIDENET,
    SDMN_TWO_TONE,
    SDMN_CLASSIC,
    MMXIX,
    SDMN_HAZARD,
    SDMN_MOO_OFF
}

fun SubMenuItemCollectionTagData.mapToDomain() = when (this) {
    SIDEMEN_ARCADE -> SubMenuItemCollectionTag.SIDEMEN_ARCADE
    SDMN_WINTER_COLLECTION -> SubMenuItemCollectionTag.SDMN_WINTER_COLLECTION
    XIX_WORLD -> SubMenuItemCollectionTag.XIX_WORLD
    SDMN_COLOUR_BLOCK -> SubMenuItemCollectionTag.SDMN_COLOUR_BLOCK
    SDMN_ACTIVEWEAR -> SubMenuItemCollectionTag.SDMN_ACTIVEWEAR
    SIDENET -> SubMenuItemCollectionTag.SIDENET
    SDMN_TWO_TONE -> SubMenuItemCollectionTag.SDMN_TWO_TONE
    SDMN_CLASSIC -> SubMenuItemCollectionTag.SDMN_CLASSIC
    MMXIX -> SubMenuItemCollectionTag.MMXIX
    SDMN_HAZARD -> SubMenuItemCollectionTag.SDMN_HAZARD
    SDMN_MOO_OFF -> SubMenuItemCollectionTag.SDMN_MOO_OFF
}

fun SubMenuItemCollectionTag.mapToData() = when (this) {
    SubMenuItemCollectionTag.SIDEMEN_ARCADE -> SIDEMEN_ARCADE
    SubMenuItemCollectionTag.SDMN_WINTER_COLLECTION -> SDMN_WINTER_COLLECTION
    SubMenuItemCollectionTag.XIX_WORLD -> XIX_WORLD
    SubMenuItemCollectionTag.SDMN_COLOUR_BLOCK -> SDMN_COLOUR_BLOCK
    SubMenuItemCollectionTag.SDMN_ACTIVEWEAR -> SDMN_ACTIVEWEAR
    SubMenuItemCollectionTag.SIDENET -> SIDENET
    SubMenuItemCollectionTag.SDMN_TWO_TONE -> SDMN_TWO_TONE
    SubMenuItemCollectionTag.SDMN_CLASSIC -> SDMN_CLASSIC
    SubMenuItemCollectionTag.MMXIX -> MMXIX
    SubMenuItemCollectionTag.SDMN_HAZARD -> SDMN_HAZARD
    SubMenuItemCollectionTag.SDMN_MOO_OFF -> SDMN_MOO_OFF
}
