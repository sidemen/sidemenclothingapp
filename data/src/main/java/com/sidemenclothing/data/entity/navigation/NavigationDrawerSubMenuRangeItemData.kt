package com.sidemenclothing.data.entity.navigation

data class NavigationDrawerSubMenuRangeItemData(
    val parentMenu: MenuItemTagData,
    val range: SubMenuItemRangeTagData
)