package com.sidemenclothing.data.entity.navigation

import com.sidemenclothing.data.entity.navigation.MenuItemTagData.ACCESSORIES
import com.sidemenclothing.data.entity.navigation.MenuItemTagData.GIFT_CARDS
import com.sidemenclothing.data.entity.navigation.MenuItemTagData.KIDS
import com.sidemenclothing.data.entity.navigation.MenuItemTagData.LOG_IN
import com.sidemenclothing.data.entity.navigation.MenuItemTagData.LOG_OUT
import com.sidemenclothing.data.entity.navigation.MenuItemTagData.MENS
import com.sidemenclothing.data.entity.navigation.MenuItemTagData.NEW
import com.sidemenclothing.data.entity.navigation.MenuItemTagData.ON_SALE
import com.sidemenclothing.data.entity.navigation.MenuItemTagData.SIDEMEN_ARCADE
import com.sidemenclothing.data.entity.navigation.MenuItemTagData.WISHLIST
import com.sidemenclothing.data.entity.navigation.MenuItemTagData.WOMENS
import com.sidemenclothing.domain.model.navigation.MenuItemTag

enum class MenuItemTagData {
    NEW,
    SIDEMEN_ARCADE,
    MENS,
    WOMENS,
    KIDS,
    ACCESSORIES,
    GIFT_CARDS,
    ON_SALE,
    WISHLIST,
    LOG_IN,
    LOG_OUT
}

fun MenuItemTagData.mapToDomain() = when (this) {
    NEW -> MenuItemTag.NEW
    SIDEMEN_ARCADE -> MenuItemTag.SIDEMEN_ARCADE
    MENS -> MenuItemTag.MENS
    WOMENS -> MenuItemTag.WOMENS
    KIDS -> MenuItemTag.KIDS
    ACCESSORIES -> MenuItemTag.ACCESSORIES
    GIFT_CARDS -> MenuItemTag.GIFT_CARDS
    ON_SALE -> MenuItemTag.ON_SALE
    WISHLIST -> MenuItemTag.WISHLIST
    LOG_IN -> MenuItemTag.LOG_IN
    LOG_OUT -> MenuItemTag.LOG_OUT
}

fun MenuItemTag.mapToData() = when (this) {
    MenuItemTag.NEW -> NEW
    MenuItemTag.SIDEMEN_ARCADE -> SIDEMEN_ARCADE
    MenuItemTag.MENS -> MENS
    MenuItemTag.WOMENS -> WOMENS
    MenuItemTag.KIDS -> KIDS
    MenuItemTag.ACCESSORIES -> ACCESSORIES
    MenuItemTag.GIFT_CARDS -> GIFT_CARDS
    MenuItemTag.ON_SALE -> ON_SALE
    MenuItemTag.WISHLIST -> WISHLIST
    MenuItemTag.LOG_IN -> LOG_IN
    MenuItemTag.LOG_OUT -> LOG_OUT
}