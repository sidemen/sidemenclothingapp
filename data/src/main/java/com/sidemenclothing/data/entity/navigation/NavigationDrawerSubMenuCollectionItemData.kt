package com.sidemenclothing.data.entity.navigation

data class NavigationDrawerSubMenuCollectionItemData(
    val parentMenu: MenuItemTagData,
    val collection: SubMenuItemCollectionTagData
)
