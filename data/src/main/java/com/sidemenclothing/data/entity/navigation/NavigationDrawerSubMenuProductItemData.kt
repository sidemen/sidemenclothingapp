package com.sidemenclothing.data.entity.navigation

data class NavigationDrawerSubMenuProductItemData(
    val parentMenu: MenuItemTagData,
    val product: SubMenuItemProductTagData
)
