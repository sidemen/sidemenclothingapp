package com.sidemenclothing.data.entity

import com.sidemenclothing.data.entity.CurrencyData.AUD
import com.sidemenclothing.data.entity.CurrencyData.CAD
import com.sidemenclothing.data.entity.CurrencyData.EUR
import com.sidemenclothing.data.entity.CurrencyData.GBP
import com.sidemenclothing.data.entity.CurrencyData.JPY
import com.sidemenclothing.data.entity.CurrencyData.USD
import com.sidemenclothing.domain.model.Currency

enum class CurrencyData(val code:String) {
    GBP("gbp"),
    USD("usd"),
    CAD("cad"),
    AUD("aud"),
    EUR("eur"),
    JPY("jpy")
}

fun CurrencyData.mapToDomain() = when (this) {
    GBP -> Currency.GBP
    USD -> Currency.USD
    CAD -> Currency.CAD
    AUD -> Currency.AUD
    EUR -> Currency.EUR
    JPY -> Currency.JPY
}

fun Currency.mapToData() = when (this) {
    Currency.GBP -> GBP
    Currency.USD -> USD
    Currency.CAD -> CAD
    Currency.AUD -> AUD
    Currency.EUR -> EUR
    Currency.JPY -> JPY
}