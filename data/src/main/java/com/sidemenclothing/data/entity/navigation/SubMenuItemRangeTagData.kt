package com.sidemenclothing.data.entity.navigation

import com.sidemenclothing.data.entity.navigation.SubMenuItemRangeTagData.ACTIVEWEAR
import com.sidemenclothing.data.entity.navigation.SubMenuItemRangeTagData.FACEMASKS
import com.sidemenclothing.data.entity.navigation.SubMenuItemRangeTagData.FOOTWEAR
import com.sidemenclothing.data.entity.navigation.SubMenuItemRangeTagData.LOUNGEWEAR
import com.sidemenclothing.data.entity.navigation.SubMenuItemRangeTagData.UNDERWEAR
import com.sidemenclothing.data.entity.navigation.SubMenuItemRangeTagData.WINTERWEAR
import com.sidemenclothing.domain.model.navigation.SubMenuItemRangeTag

enum class SubMenuItemRangeTagData {
    WINTERWEAR,
    LOUNGEWEAR,
    ACTIVEWEAR,
    FOOTWEAR,
    UNDERWEAR,
    FACEMASKS
}

fun SubMenuItemRangeTagData.mapToDomain() = when (this) {
    WINTERWEAR -> SubMenuItemRangeTag.WINTERWEAR
    LOUNGEWEAR -> SubMenuItemRangeTag.LOUNGEWEAR
    ACTIVEWEAR -> SubMenuItemRangeTag.ACTIVEWEAR
    FOOTWEAR -> SubMenuItemRangeTag.FOOTWEAR
    UNDERWEAR -> SubMenuItemRangeTag.UNDERWEAR
    FACEMASKS -> SubMenuItemRangeTag.FACEMASKS
}

fun SubMenuItemRangeTag.mapToData() = when (this) {
    SubMenuItemRangeTag.WINTERWEAR -> WINTERWEAR
    SubMenuItemRangeTag.LOUNGEWEAR -> LOUNGEWEAR
    SubMenuItemRangeTag.ACTIVEWEAR -> ACTIVEWEAR
    SubMenuItemRangeTag.FOOTWEAR -> FOOTWEAR
    SubMenuItemRangeTag.UNDERWEAR -> UNDERWEAR
    SubMenuItemRangeTag.FACEMASKS -> FACEMASKS
}