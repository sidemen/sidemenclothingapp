package com.sidemenclothing.data.entity.navigation

data class NavigationDrawerSubMenuItemData(
    val parentMenu: MenuItemTagData,
    val productsSubMenu: List<NavigationDrawerSubMenuProductItemData>,
    val rangeSubMenu: List<NavigationDrawerSubMenuRangeItemData>,
    val collectionSubMenu: List<NavigationDrawerSubMenuCollectionItemData>
)
