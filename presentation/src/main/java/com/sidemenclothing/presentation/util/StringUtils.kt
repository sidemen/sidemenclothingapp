package com.sidemenclothing.presentation.util

import java.util.Locale

fun String.toUpperCaseExceptAppName(): String {
    return this.toUpperCase(Locale.ROOT).replace("SIDEMEN CLOTHING", "Sidemen Clothing")
}