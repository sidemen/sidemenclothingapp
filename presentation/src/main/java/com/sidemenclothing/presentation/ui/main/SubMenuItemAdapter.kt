package com.sidemenclothing.presentation.ui.main

import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.sidemenclothing.domain.model.navigation.NavigationDrawerSubMenuItem
import com.sidemenclothing.presentation.R
import com.sidemenclothing.presentation.databinding.SubMenuCategoryViewBinding
import com.sidemenclothing.presentation.extension.hide
import com.sidemenclothing.presentation.extension.show

class SubMenuItemAdapter : ListAdapter<NavigationDrawerSubMenuItem, SubMenuItemAdapter.SubMenuCategoryViewHolder>(COMPARATOR) {

    private val viewPool = RecyclerView.RecycledViewPool()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SubMenuCategoryViewHolder {
        return SubMenuCategoryViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: SubMenuCategoryViewHolder, position: Int) {
        val item = getItem(position)
        return holder.bind(item, viewPool)
    }

    class SubMenuCategoryViewHolder private constructor(private val binding: SubMenuCategoryViewBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: NavigationDrawerSubMenuItem, viewPool: RecyclerView.RecycledViewPool) {
            if (item.productsSubMenu.isEmpty()) {
                binding.subMenuProductExpand.hide()
                binding.subMenuProductItems.hide()
            } else {
                binding.subMenuProductExpand.show()
                binding.subMenuProductClickable.setOnClickListener {
                    if (binding.subMenuProductClickable.isExpanded) {
                        binding.subMenuProductItems.hide()
                        binding.subMenuProductClickable.isExpanded = false
                        binding.subMenuProductExpand.startAnimation(AnimationUtils.loadAnimation(binding.subMenuProductExpand.context, R.anim.menu_drawer_retract_icon_rotate_animation))
                    } else {
                        binding.subMenuProductItems.show()
                        binding.subMenuProductClickable.isExpanded = true
                        binding.subMenuProductExpand.startAnimation(AnimationUtils.loadAnimation(binding.subMenuProductExpand.context, R.anim.menu_drawer_expand_icon_rotate_animation))
                    }

                    if (!binding.subMenuProductClickable.contentIsSet) {
                        val subMenuLayoutManager = LinearLayoutManager(
                            binding.subMenuProductItems.context,
                            RecyclerView.VERTICAL,
                            false
                        )
                        val subMenuProductItemAdapter = SubMenuProductItemAdapter()
                        binding.subMenuProductItems.apply {
                            layoutManager = subMenuLayoutManager
                            adapter = subMenuProductItemAdapter
                            setRecycledViewPool(viewPool)
                        }

                        subMenuProductItemAdapter.submitList(item.productsSubMenu)
                        binding.subMenuProductClickable.contentIsSet = true
                    }
                }
            }

            if (item.rangeSubMenu.isEmpty()) {
                binding.subMenuRangeExpand.hide()
                binding.subMenuRangeItems.hide()
            } else {
                binding.subMenuRangeExpand.show()
                binding.subMenuRangeClickable.setOnClickListener {
                    if (binding.subMenuRangeClickable.isExpanded) {
                        binding.subMenuRangeItems.hide()
                        binding.subMenuRangeClickable.isExpanded = false
                        binding.subMenuRangeExpand.startAnimation(AnimationUtils.loadAnimation(binding.subMenuRangeExpand.context, R.anim.menu_drawer_retract_icon_rotate_animation))
                    } else {
                        binding.subMenuRangeItems.show()
                        binding.subMenuRangeClickable.isExpanded = true
                        binding.subMenuRangeExpand.startAnimation(AnimationUtils.loadAnimation(binding.subMenuRangeExpand.context, R.anim.menu_drawer_expand_icon_rotate_animation))
                    }

                    if (!binding.subMenuRangeClickable.contentIsSet) {
                        val subMenuLayoutManager = LinearLayoutManager(
                            binding.subMenuRangeItems.context,
                            RecyclerView.VERTICAL,
                            false
                        )
                        val subMenuRangeItemAdapter = SubMenuRangeItemAdapter()
                        binding.subMenuRangeItems.apply {
                            layoutManager = subMenuLayoutManager
                            adapter = subMenuRangeItemAdapter
                            setRecycledViewPool(viewPool)
                        }

                        subMenuRangeItemAdapter.submitList(item.rangeSubMenu)
                        binding.subMenuRangeClickable.contentIsSet = true
                    }
                }
            }

            if (item.collectionSubMenu.isEmpty()) {
                binding.subMenuCollectionExpand.hide()
                binding.subMenuCollectionItems.hide()
            } else {
                binding.subMenuCollectionExpand.show()
                binding.subMenuCollectionClickable.setOnClickListener {
                    if (binding.subMenuCollectionClickable.isExpanded) {
                        binding.subMenuCollectionItems.hide()
                        binding.subMenuCollectionClickable.isExpanded = false
                        binding.subMenuCollectionExpand.startAnimation(AnimationUtils.loadAnimation(binding.subMenuCollectionExpand.context, R.anim.menu_drawer_retract_icon_rotate_animation))
                    } else {
                        binding.subMenuCollectionItems.show()
                        binding.subMenuCollectionClickable.isExpanded = true
                        binding.subMenuCollectionExpand.startAnimation(AnimationUtils.loadAnimation(binding.subMenuCollectionExpand.context, R.anim.menu_drawer_expand_icon_rotate_animation))
                    }

                    if (!binding.subMenuCollectionClickable.contentIsSet) {
                        val subMenuLayoutManager = LinearLayoutManager(
                            binding.subMenuCollectionItems.context,
                            RecyclerView.VERTICAL,
                            false
                        )
                        val subMenuCollectionItemAdapter = SubMenuCollectionItemAdapter()
                        binding.subMenuCollectionItems.apply {
                            layoutManager = subMenuLayoutManager
                            adapter = subMenuCollectionItemAdapter
                            setRecycledViewPool(viewPool)
                        }

                        subMenuCollectionItemAdapter.submitList(item.collectionSubMenu)
                        binding.subMenuCollectionClickable.contentIsSet = true
                    }
                }
            }
        }

        companion object {
            fun from(parent: ViewGroup): SubMenuCategoryViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = SubMenuCategoryViewBinding.inflate(layoutInflater, parent, false)
                return SubMenuCategoryViewHolder(binding)
            }
        }
    }

    companion object {
        private val COMPARATOR = object : DiffUtil.ItemCallback<NavigationDrawerSubMenuItem>() {
            override fun areItemsTheSame(
                oldItem: NavigationDrawerSubMenuItem,
                newItem: NavigationDrawerSubMenuItem
            ) = oldItem.parentMenu == newItem.parentMenu

            override fun areContentsTheSame(
                oldItem: NavigationDrawerSubMenuItem,
                newItem: NavigationDrawerSubMenuItem
            ) = oldItem == newItem

        }
    }
}