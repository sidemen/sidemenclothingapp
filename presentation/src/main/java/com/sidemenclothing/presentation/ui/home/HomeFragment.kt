package com.sidemenclothing.presentation.ui.home

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.viewModels
import com.sidemenclothing.presentation.R
import com.sidemenclothing.presentation.databinding.FragmentHomeBinding
import com.sidemenclothing.presentation.ui.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeFragment : BaseFragment() {

    private lateinit var binding: FragmentHomeBinding
    private val homeViewModel: HomeViewModel by viewModels()
    override var showToolbar: Boolean = true

    override fun onAttach(context: Context) {
        super.onAttach(context)

        requireActivity().onBackPressedDispatcher.addCallback(
            this,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    AlertDialog.Builder(requireActivity(), R.style.SidemenClothingTheme_Dialog_Alert)
                        .setTitle(R.string.exit_app_confirmation_title)
                        .setMessage(R.string.exit_app_confirmation_message)
                        .setPositiveButton(R.string.close) { _, _ ->
                            requireActivity().finish()
                        }.setNegativeButton(android.R.string.cancel) {_, _ -> }
                        .create().show()
                }
            }
        )
    }

    override fun onPause() {
        super.onPause()
        homeViewModel.showLoading()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = FragmentHomeBinding.inflate(inflater, container, false)

        homeViewModel.setShippingInformation(listOf(
            getString(R.string.worldwide_shipping),
            getString(R.string.free_shipping_international),
            getString(R.string.free_shipping_uk)
        ))

        homeViewModel.shippingInformation.observe(viewLifecycleOwner, { informationList ->
            binding.shippingInformation.startAnimation(animateInformationBanner(informationList))
        })

        return binding.root
    }

    private fun animateInformationBanner(information: List<String>, start: Int = 0) : Animation {
        var index = start
        val animation = AnimationUtils.loadAnimation(binding.shippingInformation.context, R.anim.shipping_information_animation)
        animation.setAnimationListener(object: Animation.AnimationListener {
            override fun onAnimationStart(animation: Animation?) {
                binding.shippingInformation.text = information[index % information.size]
            }

            override fun onAnimationEnd(animation: Animation?) {
                index++
                binding.shippingInformation.text = null
                binding.shippingInformation.startAnimation(animation)
            }

            override fun onAnimationRepeat(animation: Animation?) {
                TODO("Not yet implemented")
            }
        })

        return animation
    }
}