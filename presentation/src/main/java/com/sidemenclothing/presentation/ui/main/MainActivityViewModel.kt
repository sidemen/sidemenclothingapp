package com.sidemenclothing.presentation.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.sidemenclothing.domain.model.navigation.SocialMedia
import com.sidemenclothing.presentation.ui.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MainActivityViewModel @Inject constructor() : BaseViewModel() {

    private val _socialMedia = MutableLiveData<List<SocialMedia>>()
    val socialMedia: LiveData<List<SocialMedia>> = _socialMedia

    init {
        setupSocialMediaEnum()
    }

    private fun setupSocialMediaEnum() {
        _socialMedia.value = SocialMedia.values().toList()
    }
}