package com.sidemenclothing.presentation.ui.main

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.sidemenclothing.domain.model.navigation.SocialMedia
import com.sidemenclothing.presentation.BuildConfig
import com.sidemenclothing.presentation.R
import com.sidemenclothing.presentation.databinding.SocialMediaCardViewBinding

class SocialMediaAdapter :
    ListAdapter<SocialMedia, SocialMediaAdapter.SocialMediaViewHolder>(COMPARATOR) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SocialMediaViewHolder {
        return SocialMediaViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: SocialMediaViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item)
    }

    class SocialMediaViewHolder private constructor(private val binding: SocialMediaCardViewBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: SocialMedia) {
            when (item) {
                SocialMedia.FACEBOOK -> {
                    binding.socialMediaIcon.setImageResource(R.drawable.ic_facebook)
                    binding.socialMediaCard.setOnClickListener {
                        val facebookIntent = Intent(Intent.ACTION_VIEW)
                        try {
                            facebookIntent.data = Uri.parse("fb://page/" + BuildConfig.SIDEMEN_CLOTHING_FACEBOOK_ID )
                            it.context.startActivity(facebookIntent)
                        } catch (ex: ActivityNotFoundException) {
                            facebookIntent.data = Uri.parse(item.url)
                            it.context.startActivity(facebookIntent)
                        }
                    }
                }
                SocialMedia.INSTAGRAM -> {
                    binding.socialMediaIcon.setImageResource(R.drawable.ic_instagram)
                    binding.socialMediaCard.setOnClickListener {
                        val instagramIntent = Intent(Intent.ACTION_VIEW)
                        try {
                            instagramIntent.setPackage("com.instagram.android")
                            instagramIntent.data = Uri.parse(item.url)
                            it.context.startActivity(instagramIntent)
                        } catch (ex: ActivityNotFoundException) {
                            instagramIntent.data = Uri.parse(item.url)
                            it.context.startActivity(instagramIntent)
                        }
                    }
                }
                SocialMedia.TWITTER -> {
                    binding.socialMediaIcon.setImageResource(R.drawable.ic_twitter)
                    binding.socialMediaCard.setOnClickListener {
                        val twitterIntent = Intent(Intent.ACTION_VIEW)
                        try {
                            twitterIntent.setPackage("com.twitter.android")
                            twitterIntent.data = Uri.parse(item.url)
                            it.context.startActivity(twitterIntent)
                        } catch (ex: ActivityNotFoundException) {
                            twitterIntent.data = Uri.parse(item.url)
                            it.context.startActivity(twitterIntent)
                        }
                    }
                }
                SocialMedia.YOUTUBE -> {
                    binding.socialMediaIcon.setImageResource(R.drawable.ic_youtube)
                    binding.socialMediaCard.setOnClickListener {
                        val youtubeIntent = Intent(Intent.ACTION_VIEW)
                        try {
                            youtubeIntent.setPackage("com.google.android.youtube")
                            youtubeIntent.data = Uri.parse(item.url)
                            it.context.startActivity(youtubeIntent)
                        } catch (ex: ActivityNotFoundException) {
                            youtubeIntent.data = Uri.parse(item.url)
                            it.context.startActivity(youtubeIntent)
                        }
                    }
                }
            }
        }

        companion object {
            fun from(parent: ViewGroup): SocialMediaViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = SocialMediaCardViewBinding.inflate(layoutInflater, parent, false)
                return SocialMediaViewHolder(binding)
            }
        }
    }

    companion object {
        private val COMPARATOR = object : DiffUtil.ItemCallback<SocialMedia>() {
            override fun areItemsTheSame(oldItem: SocialMedia, newItem: SocialMedia) =
                oldItem.name == newItem.name

            override fun areContentsTheSame(oldItem: SocialMedia, newItem: SocialMedia) =
                oldItem == newItem
        }
    }
}