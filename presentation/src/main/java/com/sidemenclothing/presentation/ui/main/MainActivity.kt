package com.sidemenclothing.presentation.ui.main

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkRequest
import android.os.Bundle
import android.provider.Settings
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import com.google.android.material.snackbar.Snackbar
import com.sidemenclothing.domain.model.Currency
import com.sidemenclothing.domain.model.navigation.MenuItemTag
import com.sidemenclothing.domain.model.navigation.NavigationDrawerMenuItem
import com.sidemenclothing.domain.model.navigation.NavigationDrawerSubMenuItem
import com.sidemenclothing.domain.service.MenuItemsService.Companion.setupAccessoriesProductList
import com.sidemenclothing.domain.service.MenuItemsService.Companion.setupCollectionList
import com.sidemenclothing.domain.service.MenuItemsService.Companion.setupKidsProductList
import com.sidemenclothing.domain.service.MenuItemsService.Companion.setupMensProductList
import com.sidemenclothing.domain.service.MenuItemsService.Companion.setupRangeList
import com.sidemenclothing.domain.service.MenuItemsService.Companion.setupWomensProductList
import com.sidemenclothing.presentation.R
import com.sidemenclothing.presentation.databinding.ActivityMainBinding
import com.sidemenclothing.presentation.extension.hide
import com.sidemenclothing.presentation.extension.show
import com.sidemenclothing.presentation.ui.base.BaseActivity
import com.sidemenclothing.presentation.ui.base.NavigationHost
import com.sidemenclothing.presentation.util.DividerItemDecorator
import com.sidemenclothing.presentation.util.toUpperCaseExceptAppName
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

@AndroidEntryPoint
class MainActivity : BaseActivity(), NavigationHost {

    private lateinit var binding: ActivityMainBinding

    private val mainActivityViewModel: MainActivityViewModel by viewModels()

    private lateinit var appBarConfiguration: AppBarConfiguration

    private lateinit var connectivitySnackbar: Snackbar

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.SidemenClothingTheme)
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        window.statusBarColor = Color.TRANSPARENT
        window.navigationBarColor = Color.TRANSPARENT

        connectivitySnackbar =
            Snackbar.make(binding.mainContainer, R.string.no_network, Snackbar.LENGTH_INDEFINITE)

        setSupportActionBar(binding.toolbar)
        setupExitNavigationButton()
        setupMenuItemsList()
        setupSocialMediaList()
        setupCurrencyList()
        setupNavigationDrawer()
        setupNavigation()

        binding.navigationViewContactEmail.setOnClickListener {
            contactSupport()
        }

        binding.toolbarCartIcon.setOnClickListener {
            Toast.makeText(this, "WIP", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onResume() {
        super.onResume()
        setConnectivityListener()
        binding.turnOnInternetButton.setOnClickListener {
            startActivity(Intent(Settings.ACTION_WIRELESS_SETTINGS))
        }
    }

    private fun setupExitNavigationButton() {
        binding.exitNavigationDrawer.setOnClickListener {
            binding.drawerLayout.closeDrawers()
        }
    }

    private fun setupMenuItemsList() {
        val mensMenuItem = listOf(
            NavigationDrawerSubMenuItem(
                MenuItemTag.MENS, setupMensProductList(), setupRangeList(
                    MenuItemTag.MENS), setupCollectionList(MenuItemTag.MENS))
        )

        val womensMenuItem = listOf(
            NavigationDrawerSubMenuItem(
                MenuItemTag.WOMENS, setupWomensProductList(), setupRangeList(
                    MenuItemTag.WOMENS), setupCollectionList(MenuItemTag.WOMENS))
        )

        val kidsMenuItem = listOf(
            NavigationDrawerSubMenuItem(
                MenuItemTag.KIDS, setupKidsProductList(), setupRangeList(
                    MenuItemTag.KIDS), setupCollectionList(MenuItemTag.KIDS))
        )

        val accessoriesMenuItem = listOf(
            NavigationDrawerSubMenuItem(
                MenuItemTag.ACCESSORIES, setupAccessoriesProductList(), setupRangeList(
                    MenuItemTag.ACCESSORIES), setupCollectionList(MenuItemTag.ACCESSORIES))
        )

        val menuItems = listOf(
            NavigationDrawerMenuItem(MenuItemTag.NEW, emptyList()),
            NavigationDrawerMenuItem(MenuItemTag.SIDEMEN_ARCADE, emptyList()),
            NavigationDrawerMenuItem(MenuItemTag.MENS, mensMenuItem),
            NavigationDrawerMenuItem(MenuItemTag.WOMENS, womensMenuItem),
            NavigationDrawerMenuItem(MenuItemTag.KIDS, kidsMenuItem),
            NavigationDrawerMenuItem(MenuItemTag.ACCESSORIES, accessoriesMenuItem),
            NavigationDrawerMenuItem(MenuItemTag.GIFT_CARDS, emptyList()),
            NavigationDrawerMenuItem(MenuItemTag.ON_SALE, emptyList()),
            NavigationDrawerMenuItem(MenuItemTag.WISHLIST, emptyList()),
            NavigationDrawerMenuItem(MenuItemTag.LOG_IN, emptyList())
        )

        val menuItemAdapter = MenuItemAdapter()
        binding.navigationViewMenu.adapter = menuItemAdapter
        binding.navigationViewMenu.addItemDecoration(DividerItemDecorator(ContextCompat.getDrawable(binding.navigationViewMenu.context, R.drawable.highlight_divider)!!))
        menuItemAdapter.submitList(menuItems)
    }

    private fun setupSocialMediaList() {
        val socialMediaAdapter = SocialMediaAdapter()
        binding.navigationViewSocialMedia.adapter = socialMediaAdapter
        mainActivityViewModel.socialMedia.observe(this, { socialMediaList ->
            socialMediaAdapter.submitList(socialMediaList)
        })
    }

    private fun setupCurrencyList() {
        binding.currenySpinner.adapter = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, Currency.values())
    }

    private fun setupNavigationDrawer() {
        val toggle = ActionBarDrawerToggle(this, binding.drawerLayout, binding.toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        binding.drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
    }

    private fun setupNavigation() {
        val host: NavHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment?
                ?: return

        val navController = host.navController

        navController.addOnDestinationChangedListener { _, destination, _ ->
            setToolBarTitle(destination.label?.toString() ?: "")
            setToolBarIcon(null)
        }

        appBarConfiguration = AppBarConfiguration(setOf(R.id.home_screen), binding.drawerLayout)

        setupActionBarWithNavController(navController, appBarConfiguration)
    }

    private fun contactSupport() {
        intent = Intent(Intent.ACTION_SEND).apply {
            type = "message/rfc822"
            putExtra(Intent.EXTRA_EMAIL, arrayOf(getString(R.string.support_email)))
        }

        try {
            startActivity(intent)
        } catch (e: ActivityNotFoundException) {
            Toast.makeText(this, "No application available to execute action", Toast.LENGTH_LONG).show()
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp() || super.onSupportNavigateUp()
    }

    override fun setToolBarTitle(title: String?) {
        if (title != null) {
            binding.toolbarTitle.text = title.toUpperCaseExceptAppName()
            binding.toolbarTitle.show()
        } else {
            binding.toolbarTitle.hide()
        }
    }

    override fun setToolBarIcon(iconRes: Int?) {
        if (iconRes != null) {
            binding.toolbarIcon.setImageResource(iconRes)
            binding.toolbarIcon.show()
        } else {
            binding.toolbarIcon.hide()
        }
    }

    override fun showToolbar(show: Boolean) {
        binding.toolbar.isVisible = show
    }

    private fun setConnectivityListener() {
        val connectivityManager =
            applicationContext?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        if (connectivityManager.activeNetwork == null) {
            binding.connectivityBanner.show()
        }

        connectivityManager.registerNetworkCallback(NetworkRequest.Builder().build(),
            object : ConnectivityManager.NetworkCallback() {
                override fun onAvailable(network: Network) {
                    GlobalScope.launch(Dispatchers.Main) {
                        binding.connectivityBanner.hide()
                    }
                }

                override fun onLost(network: Network) {
                    GlobalScope.launch(Dispatchers.Main) {
                        binding.connectivityBanner.show()
                    }
                }
            })
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        getCurrentFragment()?.onNewIntent(intent)
    }
}