package com.sidemenclothing.presentation.ui.main

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.sidemenclothing.domain.model.navigation.NavigationDrawerSubMenuRangeItem
import com.sidemenclothing.domain.model.navigation.SubMenuItemRangeTag
import com.sidemenclothing.presentation.R
import com.sidemenclothing.presentation.databinding.SubMenuCategoryItemViewBinding

class SubMenuRangeItemAdapter :
    ListAdapter<NavigationDrawerSubMenuRangeItem, SubMenuRangeItemAdapter.SubMenuRangeItemViewHolder>(
        COMPARATOR
    ) {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): SubMenuRangeItemViewHolder {
        return SubMenuRangeItemViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: SubMenuRangeItemViewHolder, position: Int) {
        val item = getItem(position)
        return holder.bind(item)
    }

    class SubMenuRangeItemViewHolder private constructor(private val binding: SubMenuCategoryItemViewBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: NavigationDrawerSubMenuRangeItem) {
            val context = binding.root.context
            binding.subMenuCategoryItemTitle.text =  when (item.range) {
                SubMenuItemRangeTag.WINTERWEAR -> context.getString(R.string.winterwear)
                SubMenuItemRangeTag.LOUNGEWEAR -> context.getString(R.string.loungewear)
                SubMenuItemRangeTag.ACTIVEWEAR -> context.getString(R.string.activewear)
                SubMenuItemRangeTag.FOOTWEAR -> context.getString(R.string.footwear)
                SubMenuItemRangeTag.UNDERWEAR -> context.getString(R.string.underwear)
                SubMenuItemRangeTag.FACEMASKS -> context.getString(R.string.facemasks)
            }

            binding.subMenuCategoryItemClickable.setOnClickListener {
                Toast.makeText(context, "${item.parentMenu.name} -> Range -> ${binding.subMenuCategoryItemTitle.text}", Toast.LENGTH_SHORT).show()
            }
        }

        companion object {
            fun from(parent: ViewGroup): SubMenuRangeItemViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = SubMenuCategoryItemViewBinding.inflate(layoutInflater, parent, false)
                return SubMenuRangeItemViewHolder(binding)
            }
        }
    }

    companion object {
        private val COMPARATOR =
            object : DiffUtil.ItemCallback<NavigationDrawerSubMenuRangeItem>() {
                override fun areItemsTheSame(
                    oldItem: NavigationDrawerSubMenuRangeItem,
                    newItem: NavigationDrawerSubMenuRangeItem
                ) = oldItem.range == newItem.range

                override fun areContentsTheSame(
                    oldItem: NavigationDrawerSubMenuRangeItem,
                    newItem: NavigationDrawerSubMenuRangeItem
                ) = oldItem == newItem
            }
    }
}