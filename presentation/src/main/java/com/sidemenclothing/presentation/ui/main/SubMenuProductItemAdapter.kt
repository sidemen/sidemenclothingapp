package com.sidemenclothing.presentation.ui.main

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.sidemenclothing.domain.model.navigation.NavigationDrawerSubMenuProductItem
import com.sidemenclothing.domain.model.navigation.SubMenuItemProductTag
import com.sidemenclothing.presentation.R
import com.sidemenclothing.presentation.databinding.SubMenuCategoryItemViewBinding

class SubMenuProductItemAdapter :
    ListAdapter<NavigationDrawerSubMenuProductItem, SubMenuProductItemAdapter.SubMenuProductItemViewHolder>(
        COMPARATOR
    ) {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): SubMenuProductItemViewHolder {
        return SubMenuProductItemViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: SubMenuProductItemViewHolder, position: Int) {
        val item = getItem(position)
        return holder.bind(item)
    }

    class SubMenuProductItemViewHolder private constructor(private val binding: SubMenuCategoryItemViewBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: NavigationDrawerSubMenuProductItem) {
            val context = binding.root.context
            binding.subMenuCategoryItemTitle.text =  when (item.product) {
                SubMenuItemProductTag.ALL -> context.getString(R.string.all)
                SubMenuItemProductTag.HOODIES_SWEATSHIRTS -> context.getString(R.string.hoodies_sweatshirts)
                SubMenuItemProductTag.T_SHIRTS -> context.getString(R.string.t_shirts)
                SubMenuItemProductTag.JACKETS_COATS -> context.getString(R.string.jackets_coats)
                SubMenuItemProductTag.TRACKSUITS -> context.getString(R.string.tracksuits)
                SubMenuItemProductTag.LEGGINGS -> context.getString(R.string.leggings)
                SubMenuItemProductTag.SHORTS -> context.getString(R.string.shorts)
                SubMenuItemProductTag.ONESIES -> context.getString(R.string.onesies)
                SubMenuItemProductTag.PHONE -> context.getString(R.string.phone)
                SubMenuItemProductTag.HEADWEAR -> context.getString(R.string.headwear)
                SubMenuItemProductTag.WINTER -> context.getString(R.string.winter)
                SubMenuItemProductTag.BAGS -> context.getString(R.string.bags)
                SubMenuItemProductTag.GAMING -> context.getString(R.string.gaming)
                SubMenuItemProductTag.JEWELLERY -> context.getString(R.string.jewellery)
                SubMenuItemProductTag.POSTERS -> context.getString(R.string.posters)
                SubMenuItemProductTag.CUSHIONS -> context.getString(R.string.cushions)
                SubMenuItemProductTag.MENS_SALE -> context.getString(R.string.mens_sale)
                SubMenuItemProductTag.WOMENS_SALE -> context.getString(R.string.womens_sale)
                SubMenuItemProductTag.KIDS_SALE -> context.getString(R.string.kids_sale)
                SubMenuItemProductTag.ACCESSORIES_SALE -> context.getString(R.string.accessories_sale)
            }

            binding.subMenuCategoryItemClickable.setOnClickListener {
                Toast.makeText(context, "${item.parentMenu.name} -> Product -> ${binding.subMenuCategoryItemTitle.text}", Toast.LENGTH_SHORT).show()
            }
        }

        companion object {
            fun from(parent: ViewGroup): SubMenuProductItemViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = SubMenuCategoryItemViewBinding.inflate(layoutInflater, parent, false)
                return SubMenuProductItemViewHolder(binding)
            }
        }
    }

    companion object {
        private val COMPARATOR =
            object : DiffUtil.ItemCallback<NavigationDrawerSubMenuProductItem>() {
                override fun areItemsTheSame(
                    oldItem: NavigationDrawerSubMenuProductItem,
                    newItem: NavigationDrawerSubMenuProductItem
                ) = oldItem.product == newItem.product

                override fun areContentsTheSame(
                    oldItem: NavigationDrawerSubMenuProductItem,
                    newItem: NavigationDrawerSubMenuProductItem
                ) = oldItem == newItem
            }
    }
}