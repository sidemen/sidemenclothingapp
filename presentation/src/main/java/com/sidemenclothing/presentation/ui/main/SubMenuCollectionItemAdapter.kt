package com.sidemenclothing.presentation.ui.main

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.sidemenclothing.domain.model.navigation.NavigationDrawerSubMenuCollectionItem
import com.sidemenclothing.domain.model.navigation.SubMenuItemCollectionTag
import com.sidemenclothing.presentation.R
import com.sidemenclothing.presentation.databinding.SubMenuCategoryItemViewBinding

class SubMenuCollectionItemAdapter :
    ListAdapter<NavigationDrawerSubMenuCollectionItem, SubMenuCollectionItemAdapter.SubMenuCategoryItemViewHolder>(
        COMPARATOR
    ) {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): SubMenuCategoryItemViewHolder {
        return SubMenuCategoryItemViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: SubMenuCategoryItemViewHolder, position: Int) {
        val item = getItem(position)
        return holder.bind(item)
    }

    class SubMenuCategoryItemViewHolder private constructor(private val binding: SubMenuCategoryItemViewBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: NavigationDrawerSubMenuCollectionItem) {
            val context = binding.root.context
             binding.subMenuCategoryItemTitle.text =  when (item.collection) {
                 SubMenuItemCollectionTag.SIDEMEN_ARCADE -> context.getString(R.string.sidemen_arcade)
                 SubMenuItemCollectionTag.SDMN_WINTER_COLLECTION -> context.getString(R.string.sdmn_winter_collection)
                 SubMenuItemCollectionTag.XIX_WORLD -> context.getString(R.string.xix_world)
                 SubMenuItemCollectionTag.SDMN_COLOUR_BLOCK -> context.getString(R.string.sdmn_colour_block)
                 SubMenuItemCollectionTag.SDMN_ACTIVEWEAR -> context.getString(R.string.sdmn_activewear)
                 SubMenuItemCollectionTag.SIDENET -> context.getString(R.string.sidenet)
                 SubMenuItemCollectionTag.SDMN_TWO_TONE -> context.getString(R.string.sdmn_two_tone)
                 SubMenuItemCollectionTag.SDMN_CLASSIC -> context.getString(R.string.sdmn_classic)
                 SubMenuItemCollectionTag.MMXIX -> context.getString(R.string.mmxix)
                 SubMenuItemCollectionTag.SDMN_HAZARD -> context.getString(R.string.sdmn_hazard)
                 SubMenuItemCollectionTag.SDMN_MOO_OFF -> context.getString(R.string.sdmn_moo_off)
             }

            binding.subMenuCategoryItemClickable.setOnClickListener {
                Toast.makeText(context, "${item.parentMenu.name} -> Collection -> ${binding.subMenuCategoryItemTitle.text}", Toast.LENGTH_SHORT).show()
            }
        }

        companion object {
            fun from(parent: ViewGroup): SubMenuCategoryItemViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = SubMenuCategoryItemViewBinding.inflate(layoutInflater, parent, false)
                return SubMenuCategoryItemViewHolder(binding)
            }
        }
    }

    companion object {
        private val COMPARATOR =
            object : DiffUtil.ItemCallback<NavigationDrawerSubMenuCollectionItem>() {
                override fun areItemsTheSame(
                    oldItem: NavigationDrawerSubMenuCollectionItem,
                    newItem: NavigationDrawerSubMenuCollectionItem
                ) = oldItem.collection == newItem.collection

                override fun areContentsTheSame(
                    oldItem: NavigationDrawerSubMenuCollectionItem,
                    newItem: NavigationDrawerSubMenuCollectionItem
                ) = oldItem == newItem
            }
    }
}