package com.sidemenclothing.presentation.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.sidemenclothing.presentation.ui.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(): BaseViewModel() {
    
    private val _shippingInformation = MutableLiveData<List<String>>()
    val shippingInformation: LiveData<List<String>> = _shippingInformation

    fun setShippingInformation(shippingInformation: List<String>) {
        _shippingInformation.value = shippingInformation
    }
}