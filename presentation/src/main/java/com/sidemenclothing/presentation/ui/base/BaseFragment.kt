package com.sidemenclothing.presentation.ui.base

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment

abstract class BaseFragment : Fragment() {
    private var navigationHost: NavigationHost? = null
    open var showToolbar: Boolean = true

    fun setToolBarTitle(@StringRes titleRes: Int) {
        setToolBarTitle(requireContext().getString(titleRes))
    }

    fun setToolBarTitle(title: String?) {
        (activity as? NavigationHost)?.setToolBarTitle(title)
    }

    fun setToolBarIcon(toolbarIcon: Int?) {
        (activity as? NavigationHost)?.setToolBarIcon(toolbarIcon)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navigationHost?.showToolbar(showToolbar)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is NavigationHost) {
            navigationHost = context
        }
    }

    open fun onNewIntent(intent: Intent?) {
        // No-op
    }
}