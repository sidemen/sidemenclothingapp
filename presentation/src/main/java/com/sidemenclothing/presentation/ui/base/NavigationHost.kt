package com.sidemenclothing.presentation.ui.base

import androidx.annotation.DrawableRes

interface NavigationHost {
    fun setToolBarTitle(title: String?)
    fun setToolBarIcon(@DrawableRes iconRes: Int?)
    fun showToolbar(show: Boolean)
}