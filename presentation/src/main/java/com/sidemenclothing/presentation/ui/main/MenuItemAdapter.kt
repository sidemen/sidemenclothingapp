package com.sidemenclothing.presentation.ui.main

import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.sidemenclothing.domain.model.navigation.MenuItemTag
import com.sidemenclothing.domain.model.navigation.NavigationDrawerMenuItem
import com.sidemenclothing.presentation.R
import com.sidemenclothing.presentation.databinding.MenuItemViewBinding
import com.sidemenclothing.presentation.extension.hide
import com.sidemenclothing.presentation.extension.show

class MenuItemAdapter :
    ListAdapter<NavigationDrawerMenuItem, MenuItemAdapter.MenuItemViewHolder>(
        COMPARATOR
    ) {

    private val viewPool = RecyclerView.RecycledViewPool()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MenuItemViewHolder {
        return MenuItemViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: MenuItemViewHolder, position: Int) {
        val item = getItem(position)
        return holder.bind(item, viewPool)
    }

    class MenuItemViewHolder private constructor(private val binding: MenuItemViewBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: NavigationDrawerMenuItem, viewPool: RecyclerView.RecycledViewPool) {
            val context = binding.root.context
            binding.menuItemTitle.text = when (item.title) {
                MenuItemTag.NEW -> context.getString(R.string.new_clothing)
                MenuItemTag.SIDEMEN_ARCADE -> context.getString(R.string.sidemen_arcade)
                MenuItemTag.MENS -> context.getString(R.string.mens)
                MenuItemTag.WOMENS -> context.getString(R.string.womens)
                MenuItemTag.KIDS -> context.getString(R.string.kids)
                MenuItemTag.ACCESSORIES -> context.getString(R.string.accessories)
                MenuItemTag.GIFT_CARDS -> context.getString(R.string.gift_cards)
                MenuItemTag.ON_SALE -> context.getString(R.string.on_sale)
                MenuItemTag.WISHLIST -> context.getString(R.string.wishlist_x)
                MenuItemTag.LOG_IN -> {
                    binding.menuItemTitle.setTextColor(context.getColor(R.color.error_red))
                    context.getString(R.string.log_in)
                }
                MenuItemTag.LOG_OUT -> context.getString(R.string.log_out)
            }

            if (item.subMenu.isEmpty()) {
                binding.menuItemExpand.hide()
                binding.subMenuItems.hide()
            } else {
                binding.menuItemExpand.show()
                binding.menuItemClickable.setOnClickListener {
                    if (binding.menuItemClickable.isExpanded) {
                        binding.subMenuItems.hide()
                        binding.menuItemClickable.isExpanded = false
                        binding.menuItemExpand.startAnimation(AnimationUtils.loadAnimation(binding.menuItemExpand.context, R.anim.menu_drawer_retract_icon_rotate_animation))
                    } else {
                        binding.subMenuItems.show()
                        binding.menuItemClickable.isExpanded = true
                        binding.menuItemExpand.startAnimation(AnimationUtils.loadAnimation(binding.menuItemExpand.context, R.anim.menu_drawer_expand_icon_rotate_animation))
                    }

                    if (!binding.menuItemClickable.contentIsSet) {
                        val subMenuLayoutManager = LinearLayoutManager(
                            binding.subMenuItems.context,
                            RecyclerView.VERTICAL,
                            false
                        )
                        val subMenuItemAdapter = SubMenuItemAdapter()
                        binding.subMenuItems.apply {
                            layoutManager = subMenuLayoutManager
                            adapter = subMenuItemAdapter
                            setRecycledViewPool(viewPool)
                        }

                        subMenuItemAdapter.submitList(item.subMenu)
                        binding.menuItemClickable.contentIsSet = true
                    }
                }
            }
        }

        companion object {
            fun from(parent: ViewGroup): MenuItemViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = MenuItemViewBinding.inflate(layoutInflater, parent, false)
                return MenuItemViewHolder(binding)
            }
        }
    }

    companion object {
        private val COMPARATOR =
            object : DiffUtil.ItemCallback<NavigationDrawerMenuItem>() {
                override fun areItemsTheSame(
                    oldItem: NavigationDrawerMenuItem,
                    newItem: NavigationDrawerMenuItem
                ) = oldItem.title == newItem.title

                override fun areContentsTheSame(
                    oldItem: NavigationDrawerMenuItem,
                    newItem: NavigationDrawerMenuItem
                ) = oldItem == newItem
            }
    }
}