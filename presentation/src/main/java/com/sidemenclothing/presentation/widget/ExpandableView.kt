package com.sidemenclothing.presentation.widget

import android.content.Context
import android.util.AttributeSet
import android.view.View
import com.sidemenclothing.presentation.R

class ExpandableView(context: Context, attrs: AttributeSet? = null) : View(context, attrs) {

    var isExpanded: Boolean = false
    var contentIsSet: Boolean = false

    init {
        applyUserAttributes(context, attrs)
    }

    private fun applyUserAttributes(context: Context, attrs: AttributeSet?) {
        val typedArray = context.theme.obtainStyledAttributes(attrs, R.styleable.ExpandableView, 0, 0)

        isExpanded = typedArray.getBoolean(R.styleable.ExpandableView_isExpanded, false)
        contentIsSet = typedArray.getBoolean(R.styleable.ExpandableView_isContentSet, false)

        typedArray.recycle()
    }
}