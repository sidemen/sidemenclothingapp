object Versions {
    const val MAJOR_VERSION = 1
    const val MINOR_VERSION = 0
    const val BUILD_VERSION = 0

    const val MULTIPLIER_MAJOR = 10_000_000
    const val MULTIPLIER_MINOR = 100_000
    const val MULTIPLIER_BUILD = 1_000
    const val TIMEOUT_MINUTES = 60L

    const val COMPILE_SDK = 30
    const val MIN_SDK = 24
    const val TARGET_SDK = 30

    const val BUILD_TOOLS = "30.0.3"

    const val NAME: String = "$MAJOR_VERSION.$MINOR_VERSION.$BUILD_VERSION"
    const val MINOR_VERSION_NAME: String = "$MAJOR_VERSION.$MINOR_VERSION"
    val CODE: Int

    const val COIL = "1.1.1"
    const val COROUTINES = "1.4.3"
    const val DEPENDENCY_CHECKER = "0.38.0"
    const val DETEKT = "1.16.0"
    const val HILT = "2.33-beta"
    const val HILT_ANDROIDX = "1.0.0-beta01"
    const val JSOUP = "1.13.1"
    const val KOTLIN = "1.4.32"
    const val LIFECYCLE = "2.2.0"
    const val MATERIAL = "1.3.0"
    const val MULTIDEX = "2.0.1"
    const val NAVIGATION = "2.3.4"
    const val OKHTTP = "5.0.0-alpha.2"
    const val RETROFIT = "2.9.0"
    const val TEST = "4.13.2"
    const val TIMBER = "4.7.1"

    init {
        CODE =
            MAJOR_VERSION * MULTIPLIER_MAJOR + MINOR_VERSION * MULTIPLIER_MINOR + BUILD_VERSION * MULTIPLIER_BUILD
    }
}

object Plugins {
    val ANDROID = "com.android.tools.build:gradle:4.1.3"
    val DEPENDENCY_CHECKER =
        "com.github.ben-manes:gradle-versions-plugin:${Versions.DEPENDENCY_CHECKER}"
    val DETEKT = "io.gitlab.arturbosch.detekt:detekt-gradle-plugin:${Versions.DETEKT}"
    val DETEKT_KTLINT = "io.gitlab.arturbosch.detekt:detekt-formatting:${Versions.DETEKT}"
    val KOTLIN = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.KOTLIN}"
    val SAFE_ARGS = "androidx.navigation:navigation-safe-args-gradle-plugin:${Versions.NAVIGATION}"
    val HILT = "com.google.dagger:hilt-android-gradle-plugin:${Versions.HILT}"
}

object Dependencies {

    val ANDROID_TEST = arrayOf(
        "androidx.test:core:1.2.0",
        "androidx.test:runner:1.2.0",
        "androidx.test.espresso:espresso-core:3.2.0",
        "androidx.test.ext:junit:1.1.2"
    )

    val ANDROIDX = arrayOf(
        "androidx.activity:activity-ktx:1.1.0",
        "androidx.fragment:fragment-ktx:1.2.3",
        "androidx.constraintlayout:constraintlayout:2.0.4",
        "androidx.appcompat:appcompat:1.2.0",
        "androidx.legacy:legacy-support-v4:1.0.0",
        "com.google.android.material:material:${Versions.MATERIAL}",
        "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.LIFECYCLE}",
        "androidx.lifecycle:lifecycle-livedata-ktx:${Versions.LIFECYCLE}",
        "androidx.lifecycle:lifecycle-runtime-ktx:${Versions.LIFECYCLE}",
        "androidx.lifecycle:lifecycle-viewmodel-savedstate:${Versions.LIFECYCLE}"
    )

    const val ANDROIDX_KTX = "androidx.core:core-ktx:1.3.2"

    val ANDROIDX_KAPT = arrayListOf(
        "androidx.lifecycle:lifecycle-compiler:${Versions.LIFECYCLE}"
    )

    val COIL = arrayOf(
        "io.coil-kt:coil:${Versions.COIL}",
        "io.coil-kt:coil-base:${Versions.COIL}"
    )

    val COROUTINES = arrayOf(
        "org.jetbrains.kotlinx:kotlinx-coroutines-core:${Versions.COROUTINES}",
        "org.jetbrains.kotlinx:kotlinx-coroutines-android:${Versions.COROUTINES}"
    )

    val COROUTINES_DEBUG = arrayOf(
        "org.jetbrains.kotlinx:kotlinx-coroutines-debug:${Versions.COROUTINES}"
    )

    const val DESUGARING = "com.android.tools:desugar_jdk_libs:1.1.5"

    val HILT = arrayOf(
        "com.google.dagger:hilt-android:${Versions.HILT}"
    )

    val HILT_COMPILE = arrayOf(
        "com.google.dagger:hilt-android-compiler:${Versions.HILT}",
        "androidx.hilt:hilt-compiler:${Versions.HILT_ANDROIDX}"
    )

    const val HILT_WORKER = "androidx.hilt:hilt-work:${Versions.HILT_ANDROIDX}"

    const val JSOUP = "org.jsoup:jsoup:${Versions.JSOUP}"

    val KOTLIN = arrayOf(
        "org.jetbrains.kotlin:kotlin-stdlib:${Versions.KOTLIN}"
    )

    const val MOCKWS = "com.squareup.okhttp3:mockwebserver:${Versions.OKHTTP}"

    const val MULTIDEX = "androidx.multidex:multidex:${Versions.MULTIDEX}"

    val NAVIGATION = arrayOf(
        "androidx.navigation:navigation-fragment-ktx:${Versions.NAVIGATION}",
        "androidx.navigation:navigation-ui-ktx:${Versions.NAVIGATION}"
    )

    val OKHTTP = arrayOf(
        "com.squareup.okhttp3:okhttp:${Versions.OKHTTP}",
        "com.squareup.okhttp3:okhttp-bom:${Versions.OKHTTP}",
        "com.squareup.okhttp3:logging-interceptor:${Versions.OKHTTP}"
    )

    val RETROFIT = arrayOf(
        "com.squareup.retrofit2:retrofit:${Versions.RETROFIT}",
        "com.squareup.retrofit2:converter-scalars:${Versions.RETROFIT}"
    )

    // Tests
    val TEST = arrayOf(
        // Core library
        "androidx.arch.core:core-testing:2.1.0",
        "junit:junit:${Versions.TEST}"
    )

    const val TIMBER = "com.jakewharton.timber:timber:${Versions.TIMBER}"
}