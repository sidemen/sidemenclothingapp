package com.sidemenclothing.domain.model.navigation

data class NavigationDrawerSubMenuCollectionItem(
    val parentMenu: MenuItemTag,
    val collection: SubMenuItemCollectionTag
)
