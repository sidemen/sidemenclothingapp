package com.sidemenclothing.domain.model.navigation

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
enum class SocialMedia(val url: String) : Parcelable {
    FACEBOOK("https://www.facebook.com/SidemenClothing/"),
    INSTAGRAM("https://www.instagram.com/sidemenclothing/"),
    TWITTER("https://twitter.com/SidemenClothing/"),
    YOUTUBE("https://www.youtube.com/sidemen?sub_confirmation=1/")
}