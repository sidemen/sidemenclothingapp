package com.sidemenclothing.domain.model.navigation

data class NavigationDrawerSubMenuItem(
    val parentMenu: MenuItemTag,
    val productsSubMenu: List<NavigationDrawerSubMenuProductItem>,
    val rangeSubMenu: List<NavigationDrawerSubMenuRangeItem>,
    val collectionSubMenu: List<NavigationDrawerSubMenuCollectionItem>
)
