package com.sidemenclothing.domain.model.navigation

enum class MenuItemTag {
    NEW,
    SIDEMEN_ARCADE,
    MENS,
    WOMENS,
    KIDS,
    ACCESSORIES,
    GIFT_CARDS,
    ON_SALE,
    WISHLIST,
    LOG_IN,
    LOG_OUT
}