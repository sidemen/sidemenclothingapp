package com.sidemenclothing.domain.model.navigation

data class NavigationDrawerSubMenuProductItem(
    val parentMenu: MenuItemTag,
    val product: SubMenuItemProductTag
)
