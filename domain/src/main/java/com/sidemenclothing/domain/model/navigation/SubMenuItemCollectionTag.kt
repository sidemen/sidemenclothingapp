package com.sidemenclothing.domain.model.navigation

enum class SubMenuItemCollectionTag {
    SIDEMEN_ARCADE,
    SDMN_WINTER_COLLECTION,
    XIX_WORLD,
    SDMN_COLOUR_BLOCK,
    SDMN_ACTIVEWEAR,
    SIDENET,
    SDMN_TWO_TONE,
    SDMN_CLASSIC,
    MMXIX,
    SDMN_HAZARD,
    SDMN_MOO_OFF
}