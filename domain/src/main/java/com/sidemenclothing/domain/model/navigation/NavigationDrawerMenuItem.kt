package com.sidemenclothing.domain.model.navigation

data class NavigationDrawerMenuItem(
    val title: MenuItemTag,
    val subMenu: List<NavigationDrawerSubMenuItem>
)
