package com.sidemenclothing.domain.model.navigation

enum class SubMenuItemProductTag {
    ALL,
    HOODIES_SWEATSHIRTS,
    T_SHIRTS,
    JACKETS_COATS,
    TRACKSUITS,
    LEGGINGS,
    SHORTS,
    ONESIES,
    PHONE,
    HEADWEAR,
    WINTER,
    BAGS,
    GAMING,
    JEWELLERY,
    POSTERS,
    CUSHIONS,
    MENS_SALE,
    WOMENS_SALE,
    KIDS_SALE,
    ACCESSORIES_SALE
}