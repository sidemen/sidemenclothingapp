package com.sidemenclothing.domain.model.navigation

data class NavigationDrawerSubMenuRangeItem(
    val parentMenu: MenuItemTag,
    val range: SubMenuItemRangeTag
)
