package com.sidemenclothing.domain.model

import android.os.Parcelable
import com.sidemenclothing.domain.model.Currency.AUD
import com.sidemenclothing.domain.model.Currency.CAD
import com.sidemenclothing.domain.model.Currency.EUR
import com.sidemenclothing.domain.model.Currency.GBP
import com.sidemenclothing.domain.model.Currency.JPY
import com.sidemenclothing.domain.model.Currency.USD
import kotlinx.parcelize.Parcelize
import java.util.Locale

@Parcelize
enum class Currency(val code: String) : Parcelable {
    GBP("gbp"),
    USD("usd"),
    CAD("cad"),
    AUD("aud"),
    EUR("eur"),
    JPY("jpy")
}

fun Currency.getLocale(): Locale {
    return when (this) {
        GBP -> Locale.UK
        USD -> Locale.US
        CAD -> Locale.CANADA
        AUD -> Locale("en", "AU")
        EUR -> Locale.FRANCE
        JPY -> Locale.JAPAN
    }
}