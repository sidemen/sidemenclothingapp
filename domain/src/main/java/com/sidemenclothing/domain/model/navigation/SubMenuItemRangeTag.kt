package com.sidemenclothing.domain.model.navigation

enum class SubMenuItemRangeTag {
    WINTERWEAR,
    LOUNGEWEAR,
    ACTIVEWEAR,
    FOOTWEAR,
    UNDERWEAR,
    FACEMASKS
}