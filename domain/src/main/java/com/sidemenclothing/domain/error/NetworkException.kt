package com.sidemenclothing.domain.error

import java.io.IOException

class NetworkException(val code: HttpCode) : IOException()
