package com.sidemenclothing.domain.error

@Suppress("MagicNumber")
enum class CommonErrorCode(val code: Int) {
    StackOverflowError(1),
    NullPointerException(2),
    OutOfMemoryError(3),
    IllegalStateException(4),
    IllegalArgumentException(5),
    ArrayIndexOutOfBoundsException(6),
    SecurityCheck(99),
    Unknown(0);

    companion object {
        fun from(code: Int) = values().find { it.code == code } ?: Unknown
    }
}
