package com.sidemenclothing.domain.service

import com.sidemenclothing.domain.model.navigation.MenuItemTag
import com.sidemenclothing.domain.model.navigation.NavigationDrawerSubMenuCollectionItem
import com.sidemenclothing.domain.model.navigation.NavigationDrawerSubMenuProductItem
import com.sidemenclothing.domain.model.navigation.NavigationDrawerSubMenuRangeItem
import com.sidemenclothing.domain.model.navigation.SubMenuItemCollectionTag
import com.sidemenclothing.domain.model.navigation.SubMenuItemProductTag
import com.sidemenclothing.domain.model.navigation.SubMenuItemRangeTag

class MenuItemsService {
    companion object {

        //region Products

        fun setupMensProductList(): List<NavigationDrawerSubMenuProductItem> {
            return listOf(
                NavigationDrawerSubMenuProductItem(MenuItemTag.MENS, SubMenuItemProductTag.ALL),
                NavigationDrawerSubMenuProductItem(
                    MenuItemTag.MENS,
                    SubMenuItemProductTag.HOODIES_SWEATSHIRTS
                ),
                NavigationDrawerSubMenuProductItem(
                    MenuItemTag.MENS,
                    SubMenuItemProductTag.T_SHIRTS
                ),
                NavigationDrawerSubMenuProductItem(
                    MenuItemTag.MENS,
                    SubMenuItemProductTag.JACKETS_COATS
                ),
                NavigationDrawerSubMenuProductItem(
                    MenuItemTag.MENS,
                    SubMenuItemProductTag.TRACKSUITS
                ),
                NavigationDrawerSubMenuProductItem(MenuItemTag.MENS, SubMenuItemProductTag.SHORTS),
                NavigationDrawerSubMenuProductItem(MenuItemTag.MENS, SubMenuItemProductTag.ONESIES),
                NavigationDrawerSubMenuProductItem(
                    MenuItemTag.MENS,
                    SubMenuItemProductTag.MENS_SALE
                )
            )
        }

        fun setupWomensProductList(): List<NavigationDrawerSubMenuProductItem> {
            return listOf(
                NavigationDrawerSubMenuProductItem(MenuItemTag.WOMENS, SubMenuItemProductTag.ALL),
                NavigationDrawerSubMenuProductItem(
                    MenuItemTag.WOMENS,
                    SubMenuItemProductTag.HOODIES_SWEATSHIRTS
                ),
                NavigationDrawerSubMenuProductItem(
                    MenuItemTag.WOMENS,
                    SubMenuItemProductTag.T_SHIRTS
                ),
                NavigationDrawerSubMenuProductItem(
                    MenuItemTag.WOMENS,
                    SubMenuItemProductTag.LEGGINGS
                ),
                NavigationDrawerSubMenuProductItem(
                    MenuItemTag.WOMENS,
                    SubMenuItemProductTag.SHORTS
                ),
                NavigationDrawerSubMenuProductItem(
                    MenuItemTag.WOMENS,
                    SubMenuItemProductTag.WOMENS_SALE
                )
            )
        }

        fun setupKidsProductList(): List<NavigationDrawerSubMenuProductItem> {
            return listOf(
                NavigationDrawerSubMenuProductItem(MenuItemTag.KIDS, SubMenuItemProductTag.ALL),
                NavigationDrawerSubMenuProductItem(
                    MenuItemTag.KIDS,
                    SubMenuItemProductTag.HOODIES_SWEATSHIRTS
                ),
                NavigationDrawerSubMenuProductItem(
                    MenuItemTag.KIDS,
                    SubMenuItemProductTag.T_SHIRTS
                ),
                NavigationDrawerSubMenuProductItem(
                    MenuItemTag.KIDS,
                    SubMenuItemProductTag.JACKETS_COATS
                ),
                NavigationDrawerSubMenuProductItem(
                    MenuItemTag.KIDS,
                    SubMenuItemProductTag.TRACKSUITS
                ),
                NavigationDrawerSubMenuProductItem(
                    MenuItemTag.KIDS,
                    SubMenuItemProductTag.KIDS_SALE
                )
            )
        }

        fun setupAccessoriesProductList(): List<NavigationDrawerSubMenuProductItem> {
            return listOf(
                NavigationDrawerSubMenuProductItem(
                    MenuItemTag.ACCESSORIES,
                    SubMenuItemProductTag.ALL
                ),
                NavigationDrawerSubMenuProductItem(
                    MenuItemTag.ACCESSORIES,
                    SubMenuItemProductTag.PHONE
                ),
                NavigationDrawerSubMenuProductItem(
                    MenuItemTag.ACCESSORIES,
                    SubMenuItemProductTag.HEADWEAR
                ),
                NavigationDrawerSubMenuProductItem(
                    MenuItemTag.ACCESSORIES,
                    SubMenuItemProductTag.WINTER
                ),
                NavigationDrawerSubMenuProductItem(
                    MenuItemTag.ACCESSORIES,
                    SubMenuItemProductTag.BAGS
                ),
                NavigationDrawerSubMenuProductItem(
                    MenuItemTag.ACCESSORIES,
                    SubMenuItemProductTag.GAMING
                ),
                NavigationDrawerSubMenuProductItem(
                    MenuItemTag.ACCESSORIES,
                    SubMenuItemProductTag.JEWELLERY
                ),
                NavigationDrawerSubMenuProductItem(
                    MenuItemTag.ACCESSORIES,
                    SubMenuItemProductTag.POSTERS
                ),
                NavigationDrawerSubMenuProductItem(
                    MenuItemTag.ACCESSORIES,
                    SubMenuItemProductTag.CUSHIONS
                ),
                NavigationDrawerSubMenuProductItem(
                    MenuItemTag.ACCESSORIES,
                    SubMenuItemProductTag.ACCESSORIES_SALE
                )
            )
        }
        //endregion

        //region Range

        fun setupRangeList(tag: MenuItemTag): List<NavigationDrawerSubMenuRangeItem> {
            return listOf(
                NavigationDrawerSubMenuRangeItem(tag, SubMenuItemRangeTag.WINTERWEAR),
                NavigationDrawerSubMenuRangeItem(tag, SubMenuItemRangeTag.LOUNGEWEAR),
                NavigationDrawerSubMenuRangeItem(tag, SubMenuItemRangeTag.ACTIVEWEAR),
                NavigationDrawerSubMenuRangeItem(tag, SubMenuItemRangeTag.FOOTWEAR),
                NavigationDrawerSubMenuRangeItem(tag, SubMenuItemRangeTag.UNDERWEAR),
                NavigationDrawerSubMenuRangeItem(tag, SubMenuItemRangeTag.FACEMASKS)
            )
        }
        //endregion

        //region Collection

        fun setupCollectionList(tag: MenuItemTag): List<NavigationDrawerSubMenuCollectionItem> {
            return listOf(
                NavigationDrawerSubMenuCollectionItem(
                    tag,
                    SubMenuItemCollectionTag.SIDEMEN_ARCADE
                ),
                NavigationDrawerSubMenuCollectionItem(
                    tag,
                    SubMenuItemCollectionTag.SDMN_WINTER_COLLECTION
                ),
                NavigationDrawerSubMenuCollectionItem(
                    tag,
                    SubMenuItemCollectionTag.XIX_WORLD
                ),
                NavigationDrawerSubMenuCollectionItem(
                    tag,
                    SubMenuItemCollectionTag.SDMN_COLOUR_BLOCK
                ),
                NavigationDrawerSubMenuCollectionItem(
                    tag,
                    SubMenuItemCollectionTag.SDMN_ACTIVEWEAR
                ),
                NavigationDrawerSubMenuCollectionItem(
                    tag,
                    SubMenuItemCollectionTag.SIDENET
                ),
                NavigationDrawerSubMenuCollectionItem(
                    tag,
                    SubMenuItemCollectionTag.SDMN_TWO_TONE
                ),
                NavigationDrawerSubMenuCollectionItem(
                    tag,
                    SubMenuItemCollectionTag.SDMN_CLASSIC
                ),
                NavigationDrawerSubMenuCollectionItem(
                    tag,
                    SubMenuItemCollectionTag.MMXIX
                ),
                NavigationDrawerSubMenuCollectionItem(
                    tag,
                    SubMenuItemCollectionTag.SDMN_HAZARD
                ),
                NavigationDrawerSubMenuCollectionItem(
                    tag,
                    SubMenuItemCollectionTag.SDMN_MOO_OFF
                )
            )
        }
        //endregion
    }
}